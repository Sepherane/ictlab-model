﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour {

    public GameObject cameraFocus;
    private Vector3 previousMouse;
    private float dragspeed = 0.15f;
    private float scrollSpeed = 50;
	
	// Update is called once per frame
	void Update () {
        HandleDrag();
        HandleScroll();
    }

    void HandleDrag()
    {
        if (Input.GetMouseButtonDown(0))
        {
            previousMouse = Input.mousePosition;
            return;
        }

        if (!Input.GetMouseButton(0)) return;

        Vector3 newRotation = new Vector3((Input.mousePosition.y - previousMouse.y) * -dragspeed + cameraFocus.transform.localEulerAngles.x, (Input.mousePosition.x - previousMouse.x) * dragspeed + cameraFocus.transform.localEulerAngles.y, 0);

        cameraFocus.transform.localEulerAngles = newRotation;
        previousMouse = Input.mousePosition;
    }

    void HandleScroll()
    {
        var scroll = Input.GetAxis("Mouse ScrollWheel");
        if (scroll > 0f)
        {
            // Zoom in
            gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z + scrollSpeed * scroll);
        }
        else if (scroll < 0f)
        {
            // Zoom out
            gameObject.transform.localPosition = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z + scrollSpeed * scroll);
        }
    }


}