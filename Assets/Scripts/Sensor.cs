﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    Color COLD = new Color32(0, 0, 255, 255);
    Color MEDIUM = new Color32(242, 158, 14, 255);
    Color HOT = new Color32(255, 0, 0, 255);

    public void SetTemperature(float value)
    {
        Light light = GetComponent<Light>();
        if (value > 20)
        {
            light.color = HOT;
        }
        else if (value > 15)
        {
            light.color = MEDIUM;
        }
        else
            light.color = COLD;
    }
}