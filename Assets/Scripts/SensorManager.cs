﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;

public class SensorManager : MonoBehaviour
{

    public GameObject[] floors;
    public GameObject sensor;
    private Vector2[] sensorOffsets = new Vector2[] { new Vector2(400, 200), new Vector2(400, 200), new Vector2(400, 200) };
    private float scaleFactor = 0.33f;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(GetSensors());
    }

    void ShowSensors(string data)
    {
        var json = JSON.Parse(data);
        int i = 0;
        while (json[i] != null)
        {
            int floor = json[i]["floor"].AsInt;
            Vector2 position = new Vector2(json[i]["position"]["x"].AsInt, json[i]["position"]["y"].AsInt);

            Vector3 sensorPosition = CalculatePosition(floor, position);

            GameObject newSensor = Instantiate(sensor, sensorPosition, Quaternion.identity);
            newSensor.transform.parent = this.transform;
            newSensor.transform.localPosition = sensorPosition;

            Sensor sensorScript = newSensor.GetComponent<Sensor>();

            int datatype = 0;
            while(json[i]["dataTypes"][datatype] != null)
            {
                string dataType = json[i]["dataTypes"][datatype];
                if (dataType == "temperature")
                    sensorScript.SetTemperature(json[i]["lastMeasurement"][datatype].AsFloat);

                datatype++;
            }

            i++;
        }
    }

    Vector3 CalculatePosition(int floor, Vector2 position)
    {
        Vector3 sensorPos = new Vector3();

        sensorPos.y = floors[floor].transform.localPosition.y;
        sensorPos.x = floors[floor].transform.localPosition.x - (position.x - sensorOffsets[floor].x) * scaleFactor;
        sensorPos.z = floors[floor].transform.localPosition.z + (position.y - sensorOffsets[floor].y) * scaleFactor;

        return sensorPos;
    }

    IEnumerator GetSensors()
    {
        UnityWebRequest www = UnityWebRequest.Get("http://146.185.142.242/api/sensors");
        yield return www.Send();

        if (www.isError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            ShowSensors(www.downloadHandler.text);
        }
    }
}
